﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Thinktecture.IdentityServer.Core.Models;

namespace GuitarJam.IdService.Config
{
    public static class Scopes
    {
        public static IEnumerable<Scope> Get()
        {
            List<Scope> scopes = new List<Scope>
            {
                StandardScopes.OpenId,
                StandardScopes.Profile
            };

            return scopes;
        }
    }
}