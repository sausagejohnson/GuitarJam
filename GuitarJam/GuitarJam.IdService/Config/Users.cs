﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web;
using Thinktecture.IdentityServer.Core;
using Thinktecture.IdentityServer.Core.Services.InMemory;

namespace GuitarJam.IdService.Config
{
    public static class Users
    {
        public static List<InMemoryUser> Get()
        {
            return new List<InMemoryUser>()
            {
                new InMemoryUser
                {
                    Username = "ralph",
                    Password = "password",
                    Subject = "1",

                    Claims = new[]
                    {
                        new Claim(Constants.ClaimTypes.GivenName, "ralph"),
                        new Claim(Constants.ClaimTypes.FamilyName, "brown")
                    }
                },

                new InMemoryUser
                {
                    Username = "alfronzo",
                    Password = "password",
                    Subject = "2",

                    Claims = new[]
                    {
                        new Claim(Constants.ClaimTypes.GivenName, "alfronzo"),
                        new Claim(Constants.ClaimTypes.FamilyName, "ramerez")
                    }
                }
            };
        }
    }
}