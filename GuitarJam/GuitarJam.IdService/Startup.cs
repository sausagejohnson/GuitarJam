﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.Owin;
using Owin;
using Thinktecture.IdentityServer.Core.Configuration;
using GuitarJam.IdService.Config;

[assembly: OwinStartup(typeof(GuitarJam.IdService.Startup))]

namespace GuitarJam.IdService
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            app.Map("/identity", idSrvApp =>
            {
                idSrvApp.UseIdentityServer(new Thinktecture.IdentityServer.Core.Configuration.IdentityServerOptions
                {
                    SiteName = "Embedded IdentityServer",
                    IssuerUri = "https://openidserver.web/embedded",

                    Factory = InMemoryFactory.Create(
                        users: Users.Get(),
                        clients: Clients.Get(),
                        scopes: Scopes.Get()
                    )
                });
            });
        }
    }
}