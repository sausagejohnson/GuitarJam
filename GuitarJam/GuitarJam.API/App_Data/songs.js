[
    {
        'trackid': 1,
        'musickey': 1,
        'song': 'Summer of 81',
        'artist': 'Mondo Rock',
        'youtube': 'https://www.youtube.com/watch?v=rSHNR3y9www',
        'chords': 'https://tabs.ultimate-guitar.com/m/midnight_oil/the_dead_heart_crd.htm'
    },
    {
        'trackid': 2,
        'musickey': 5,
        'song': 'The Dead Heart 2',
        'artist': 'Midnight Oil',
        'youtube': 'https://www.youtube.com/watch?v=rSHNR3y9www',
        'chords': 'https://tabs.ultimate-guitar.com/m/midnight_oil/the_dead_heart_crd.htm'
    }
]
