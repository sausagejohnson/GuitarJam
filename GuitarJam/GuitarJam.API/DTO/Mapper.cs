﻿using GuitarJam.API.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GuitarJam.API.DTO
{
    public class Mapper
    {
        public TrackDTO Map(GuitarJam.API.Entities.Track input)
        {
            TrackDTO result = new TrackDTO()
            {
                PublicKey = input.PublicKey,
                Chords = input.Chords,
                MusicKey = MusicKeyForDisplay(input.MusicKey),
                Song = input.Song,
                Artist = input.Artist,
                Youtube = input.Youtube
            };

            return result;
        }

        public List<TrackDTO> Map(List<GuitarJam.API.Entities.Track> input)
        {
            List<TrackDTO> result = new List<TrackDTO>();
            foreach (GuitarJam.API.Entities.Track t in input)
            {
                result.Add(
                    Map(t)
                );
            }
            return result;
        }


        public string MusicKeyForDisplay(MusicKeys key)
        {
            return key.ToString().Replace("Sharp", "#");
        }

        public string MusicKeyForDisplay(int key)
        {
            string musicKeyName = Enum.GetName(typeof(MusicKeys), key);
            return musicKeyName.Replace("Sharp", "#");
        }
    }

}