namespace GuitarJam.API.DTO
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class TrackDTO
    {
        public Guid PublicKey { get; set; }

        [Required]
        public string Song { get; set; }

        [Required]
        public string Artist { get; set; }

        [Required]
        public string MusicKey { get; set; }

        [Required]
        public string Youtube { get; set; }

        [Required]
        public string Chords { get; set; }

    }
}
