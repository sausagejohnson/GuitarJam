﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using GuitarJam.API;
using GuitarJam.API.Entities;

namespace GuitarJam.API.Repositories
{
    public class TrackRepository : ITrackRepository
    {
        public List<GuitarJam.API.Entities.Track> GetAllTracks()
        {
            GuitarJamEntities dbContext = new GuitarJamEntities();

            var trax = dbContext.Tracks.ToList();

            IQueryable<GuitarJam.API.Entities.Track> tracks = from track in dbContext.Tracks
                                                     select new GuitarJam.API.Entities.Track
                                                     {
                                                         Chords = track.Chords,
                                                         Song = track.Song,
                                                         Artist = track.Artist,
                                                         MusicKey = track.MusicKey ?? 1,
                                                         Youtube = track.Youtube,
                                                         PublicKey = track.PublicKey
                                                     };

            return tracks.ToList();
        }

        public void SaveTrack(Track track)
        {
            //TraxEntities entities = new TraxEntities();

            //GuitarJam.API.Track trackToSave = new GuitarJam.API.Track();
            //trackToSave.Song = track.Song;
            //trackToSave.Artist = track.Artist;
            //trackToSave.Youtube = track.Youtube;
            //trackToSave.Chords = track.Chords;
            //trackToSave.MusicKey = track.MusicKey;
            //trackToSave.PublicKey = new Guid();

            //entities.Tracks.Add(trackToSave);
            //entities.SaveChanges();
        }

    }
}