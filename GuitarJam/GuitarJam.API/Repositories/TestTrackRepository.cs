﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using GuitarJam.API.Entities;
using Newtonsoft.Json;

namespace GuitarJam.API.Repositories
{
    public class TestTrackRepository : ITrackRepository
    {
        public List<GuitarJam.API.Entities.Track> GetAllTracks()
        {
            string appDataFolder = HttpContext.Current.Server.MapPath("~/App_Data");

            StreamReader reader = new StreamReader(appDataFolder + @"/songs.js");
            string data = reader.ReadToEnd();

            List<GuitarJam.API.Entities.Track> trackData = JsonConvert.DeserializeObject<List<GuitarJam.API.Entities.Track>>(data);

            //trackData.Add(new Track
            //{
            //    Song = "Ralph",
            //    Chords = "http://asdf",
            //    PublicKey = new Guid(),
            //    Youtube = "http://yt"
            //});

            return trackData.ToList();
        }

        public void SaveTrack(Track track)
        {
            return;
        }

    }
}