﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using GuitarJam.API.Entities;

namespace GuitarJam.API.Repositories
{
    public interface ITrackRepository
    {
        List<GuitarJam.API.Entities.Track> GetAllTracks();

        void SaveTrack(Track track);

    }
}