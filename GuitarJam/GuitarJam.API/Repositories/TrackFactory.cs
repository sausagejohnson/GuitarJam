﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;

namespace GuitarJam.API.Repositories
{
    static class TrackFactory
    {
        public static ITrackRepository Get()
        {
            bool isTestData = Convert.ToBoolean(ConfigurationManager.AppSettings["TestData"].ToString());

            if (isTestData)
            {
                return new TestTrackRepository();
            } else
            {
                return new TrackRepository();
            }
        }
    }
}