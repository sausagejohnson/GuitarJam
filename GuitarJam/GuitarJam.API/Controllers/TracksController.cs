﻿using GuitarJam.API.Repositories;
using GuitarJam.API.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using GuitarJam.API.DTO;

namespace GuitarJam.API.Controllers
{
    [RoutePrefix("api")]
    public class TracksController : ApiController
    {
        [HttpGet]
        [Route("tracks")]
        public IHttpActionResult GetAllTracks()
        {

            try
            {
                List<GuitarJam.API.Entities.Track> tracks = new List<GuitarJam.API.Entities.Track>();
                ITrackRepository repo = TrackFactory.Get();
                tracks = repo.GetAllTracks().OrderBy(x => x.Song).ToList();

                Mapper mapper = new Mapper();
                List<TrackDTO> dtos = mapper.Map(tracks);

                return Ok(dtos);
            }
            catch (Exception ex)
            {
                return InternalServerError();
            }

        }

        [HttpPost]
        [Route("tracks")]
        public IHttpActionResult SaveTrack([FromBody] Track track)
        {

            TrackRepository repo = new TrackRepository();
            repo.SaveTrack(track);

            List<GuitarJam.API.Entities.Track> tracks = new List<GuitarJam.API.Entities.Track>();
            tracks = repo.GetAllTracks();

            //return Ok(tracks);
            return Created<List<GuitarJam.API.Entities.Track>>(RequestContext.Url.ToString(), tracks);
        }
    }
}
