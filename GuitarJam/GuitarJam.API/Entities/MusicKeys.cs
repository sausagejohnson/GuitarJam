﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GuitarJam.API.Entities
{
    public enum MusicKeys
    {
        C,
        CSharp,
        D,
        DSharp,
        E,
        F,
        FSharp,
        G,
        GSharp,
        A,
        ASharp,
        B
    }
}