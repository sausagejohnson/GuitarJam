﻿using GuitarJam.API.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GuitarJam.API.Entities
{

    public class Track
    {
        public int TrackId { get; set; }
        public string Artist { get; set; }
        public string Song { get; set; }
        public string Youtube { get; set; }
        public string Chords { get; set; }
        public int MusicKey { get; set; }
        public Guid PublicKey { get; set; }

    }
}