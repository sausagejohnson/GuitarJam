//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace GuitarJam.API
{
    using System;
    using System.Collections.Generic;
    
    public partial class Selection
    {
        public int SelectionId { get; set; }
        public System.Guid UserId { get; set; }
        public int TrackId { get; set; }
    }
}
