﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using GuitarJam.API.Entities;
using GuitarJam.API.DTO;

namespace GuitarJam.Tests.Controllers
{
    [TestClass]
    public class TrackTests
    {
        [TestMethod]
        public void Display_MusicKey_For_0_ShouldBe_C()
        {
            // Arrange
            Mapper map = new Mapper();

            // Act
            string musicKey = map.MusicKeyForDisplay(0);

            // Assert
            Assert.AreEqual(musicKey, "C");
        }

        [TestMethod]
        public void Display_MusicKey_For_1_ShouldBe_C_Sharp()
        {
            // Arrange
            Mapper map = new Mapper();

            // Act
            string musicKey = map.MusicKeyForDisplay(1);

            // Assert
            Assert.AreEqual(musicKey, "C#");
        }

        [TestMethod]
        public void Display_MusicKey_For_4_ShouldBe_E()
        {
            // Arrange
            Mapper map = new Mapper();

            // Act
            string musicKey = map.MusicKeyForDisplay(4);

            // Assert
            Assert.AreEqual(musicKey, "E");
        }

        [TestMethod]
        public void Display_MusicKey_For_8_ShouldBe_G_Sharp()
        {
            // Arrange
            Mapper map = new Mapper();

            // Act
            string musicKey = map.MusicKeyForDisplay(8);

            // Assert
            Assert.AreEqual(musicKey, "G#");
        }

        [TestMethod]
        public void Display_MusicKey_For_11_ShouldBe_B()
        {
            // Arrange
            Mapper map = new Mapper();

            // Act
            string musicKey = map.MusicKeyForDisplay(11);

            // Assert
            Assert.AreEqual(musicKey, "B");
        }

        [TestMethod]
        public void Display_MusicKey_For_DSharp_ShouldBe_DSharp()
        {
            // Arrange
            Mapper map = new Mapper();

            // Act
            string musicKey = map.MusicKeyForDisplay(MusicKeys.DSharp);

            // Assert
            Assert.AreEqual(musicKey, "D#");
        }



        [TestMethod]
        public void Display_MusicKey_For_G_ShouldBe_G()
        {
            // Arrange
            Mapper map = new Mapper();

            // Act
            string musicKey = map.MusicKeyForDisplay(MusicKeys.G);

            // Assert
            Assert.AreEqual(musicKey, "G");
        }

        [TestMethod]
        public void Display_MusicKey_For_ASharp_ShouldBe_ASharp()
        {
            // Arrange
            Mapper map = new Mapper();

            // Act
            string musicKey = map.MusicKeyForDisplay(MusicKeys.ASharp);

            // Assert
            Assert.AreEqual(musicKey, "A#");
        }

    }

}
