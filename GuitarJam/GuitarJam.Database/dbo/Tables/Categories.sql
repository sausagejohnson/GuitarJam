﻿CREATE TABLE [dbo].[Categories] (
    [CategoryId]   INT        NOT NULL,
    [CategoryType] NCHAR (10) NULL,
    CONSTRAINT [PK_Categories] PRIMARY KEY CLUSTERED ([CategoryId] ASC)
);



