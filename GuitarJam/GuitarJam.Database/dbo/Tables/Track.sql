﻿CREATE TABLE [dbo].[Track] (
    [TrackId]         INT              IDENTITY (1, 1) NOT NULL,
    [Song]            VARCHAR (MAX)    NOT NULL,
    [Youtube]         VARCHAR (MAX)    NOT NULL,
    [Chords]          VARCHAR (MAX)    NOT NULL,
    [PublicKey]       UNIQUEIDENTIFIER NOT NULL,
    [PrivateToUserId] UNIQUEIDENTIFIER NULL,
    [CategoryId]      INT              NULL,
    [Artist]          VARCHAR (MAX)    NULL,
    [MusicKey]        INT              NULL,
    CONSTRAINT [PK_Track] PRIMARY KEY CLUSTERED ([TrackId] ASC),
    CONSTRAINT [FK_Track_Categories] FOREIGN KEY ([CategoryId]) REFERENCES [dbo].[Categories] ([CategoryId]) NOT FOR REPLICATION,
    CONSTRAINT [FK_Track_Users] FOREIGN KEY ([PrivateToUserId]) REFERENCES [dbo].[Users] ([UserId])
);

