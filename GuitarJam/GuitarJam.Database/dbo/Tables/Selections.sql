﻿CREATE TABLE [dbo].[Selections] (
    [SelectionId] INT              IDENTITY (1, 1) NOT NULL,
    [UserId]      UNIQUEIDENTIFIER NOT NULL,
    [TrackId]     INT              NOT NULL,
    CONSTRAINT [PK_Selections] PRIMARY KEY CLUSTERED ([SelectionId] ASC)
);



