﻿CREATE TABLE [dbo].[Users] (
    [UserId]   UNIQUEIDENTIFIER CONSTRAINT [DF_Users_UserId] DEFAULT (newid()) NOT NULL,
    [Username] NCHAR (100)      NOT NULL,
    [Password] NCHAR (100)      NOT NULL,
    CONSTRAINT [PK_Users] PRIMARY KEY CLUSTERED ([UserId] ASC)
);



