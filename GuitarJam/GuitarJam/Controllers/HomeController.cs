﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;
using GuitarJam.Models;
using Newtonsoft.Json;
using RestSharp;

namespace GuitarJam.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }



        [HttpGet]
        public JsonResult GetList()
        {
            RestClient client = new RestClient(ConfigurationManager.AppSettings["GuitarApiUrl"]);
            RestRequest request = new RestRequest("api/tracks");

            IRestResponse response = client.Get(request);

            List<TrackDTO> tracks = JsonConvert.DeserializeObject<List<TrackDTO>>(response.Content); //Ask Terry did he have to get around this issue by mashing RestSharp and Newtonsoft serializers?

            JsonResult result = new JsonResult();
            result.JsonRequestBehavior = JsonRequestBehavior.AllowGet;
            result.Data = tracks;

            return result;
        }

        [HttpPost]
        public JsonResult SaveTrack(int id, string song, string youtube, string chords, string musickey)
        {
            TrackDTO track = new TrackDTO
            {
                id = id,
                Song = song,
                Youtube = youtube,
                Chords = chords,
                MusicKey = musickey
            };

            string trackJson = JsonConvert.SerializeObject(track);
            StringContent utfTrackJson = new StringContent(trackJson, System.Text.Encoding.Unicode, "application/json");

            RestClient client = new RestClient(ConfigurationManager.AppSettings["GuitarApiUrl"]);
            RestRequest request = new RestRequest("api/tracks", Method.POST);
            request.AddJsonBody(track);

            IRestResponse response = client.Post(request);

            List<TrackDTO> tracks = JsonConvert.DeserializeObject<List<TrackDTO>>(response.Content); //Ask Terry did he have to get around this issue by mashing RestSharp and Newtonsoft serializers?

            JsonResult result = new JsonResult();
            result.JsonRequestBehavior = JsonRequestBehavior.AllowGet;
            result.Data = tracks;

            return result;
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}