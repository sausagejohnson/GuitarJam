function loadSong(youtubeCode, chordsFragment) {
    $('#youtube')[0].firstChild.src = 'http://www.youtube.com/embed/' + youtubeCode + '?autoplay=1';
    $('#guitar-tabs')[0].firstChild.src = getTabLink(chordsFragment);
}

function getTabLink(chordsFragment) {
    if (chordsFragment.startsWith('http')) {
        return chordsFragment;
    }

    return 'https://tabs.ultimate-guitar.com/' + chordsFragment;
}

function renderSongList(data) {
    $('#song-list').empty();

    var html = '<ul>';

    for (var x = 0; x < data.length; x++) {
        var entry = data[x];
        html += '<li><a href="#" onclick="loadSong(\'' + entry.Youtube + '\', \''
            + entry.Chords + '\')">' + entry.Song + ' - ' + entry.Artist + ' (Key: ' + entry.MusicKey + ')' + '</a></li>';
    }

    html += '</ul>';

    $('#song-list').append(html);
}

function loadSongList() {
    $('#song-list').hide();
    $('#loader').show();
    $.get('Home/GetList', '', function (result) {
        renderSongList(result);
        $('#song-list').show();
        $('#loader').hide();
    }, 'json');
}

function openControlPanel() {
    $('#marker').addClass('top');
    $('#controls').addClass('top');
}

function closeControlPanel() {
    $('#marker').removeClass('top');
    $('#controls').removeClass('top');
}


function toggle(){
    if ($('#marker').hasClass('top')) {
        closeControlPanel();
    } else {
        loadSongList();
        openControlPanel();
    }
}

$('#controls').click(function () {
    toggle();
    $("#add-track-popup").fadeOut();
});

$('#add-button').click(function (event) {
    event.stopPropagation();
    $("#add-track-popup").fadeIn();
});

$('#save-button').click(function (event) {
    $("#add-track-popup").fadeOut();

    var data = {
        song: $('#song').val(),
        youtube: $('#you-tube').val(),
        chords: $('#chords').val()
    };
    $.post('Home/SaveTrack', data, function (response) {
        renderSongList(response);
    }, 'json')
});
