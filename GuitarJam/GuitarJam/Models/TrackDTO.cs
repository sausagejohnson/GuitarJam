﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GuitarJam.Models
{
    public class TrackDTO
    {
        public int id { get; set; }
        public string MusicKey { get; set; }
        public string Song { get; set; }
        public string Artist { get; set; }
        public string Youtube { get; set; }
        public string Chords { get; set; }
    }
}