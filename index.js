function loadSong(youtubeUrl, chordsUrl){
  $('#youtube')[0].firstChild.src = youtubeUrl;
  $('#guitar-tabs')[0].firstChild.src = chordsUrl;
}

function toggle(){
  if ($('#marker').hasClass('top')){
    $('#marker').removeClass('top');
    $('#controls').removeClass('top');
  } else {
    $('#marker').addClass('top');
    $('#controls').addClass('top');
  }

}

$('#controls').click(toggle);
